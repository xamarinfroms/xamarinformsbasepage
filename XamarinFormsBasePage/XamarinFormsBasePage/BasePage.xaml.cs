﻿
using Xamarin.Forms;

namespace XamarinFormsBasePage
{



    [ContentProperty("Children")]
    public partial class BasePage : ContentPage
    {

        public View Children
        {
            get { return this.children; }
            set
            {
                this.children = value;
                this.layoutContent.Children.Add(value);
            }
        }
        private View children;

        public BasePage()
        {
            InitializeComponent();
        }
    }
}